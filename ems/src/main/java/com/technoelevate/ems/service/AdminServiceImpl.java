package com.technoelevate.ems.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.technoelevate.ems.dao.AdminDao;
import com.technoelevate.ems.dao.EmployeeDao;
import com.technoelevate.ems.dto.AdminDto;
import com.technoelevate.ems.dto.EmployeeDto;

@Service
public class AdminServiceImpl implements AdminService {
	
	
	@Autowired
	AdminDao dao;
	
	

	@Override
	public AdminDto register(AdminDto dto) {
		if(dto.getPassword().equals(dto.getConfirm_password()))
			return  dao.save(dto);
		else
		return null;
	}

	@Override
	public AdminDto signIn(AdminDto dto) {
		try {
			AdminDto empId = dao.findByEmailId(dto.getEmailId());
			return empId;
		} catch (Exception e) {
			return null;		}
	
	}

	@Override
	public AdminDto forgotPassword(AdminDto dto) {
		AdminDto empId = dao.findByEmailId(dto.getEmailId());
		if(empId==null) {
			return null;
		}
			
		if(empId.getEmailId().equals(dto.getEmailId())) {
			
			empId.setEmailId(empId.getEmailId());
			empId.setName(empId.getName());
			empId.setUserName(empId.getUserName());
			empId.setPassword(dto.getPassword());
			empId.setConfirm_password(dto.getPassword());
			dao.save(empId);
			return empId;
		}else {
			return null;
		}		
	
	}

	
}
