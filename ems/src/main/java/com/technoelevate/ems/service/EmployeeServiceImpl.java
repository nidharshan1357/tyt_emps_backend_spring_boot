package com.technoelevate.ems.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.technoelevate.ems.dao.EmployeeDao;
import com.technoelevate.ems.dto.EmployeeDto;

@Service
public class EmployeeServiceImpl implements EmployeeService{
	
	@Autowired
	EmployeeDao dao;	

	@Override
	public EmployeeDto empRegister(EmployeeDto dto2) {
		return dao.save(dto2);	
}

	@Override
	public EmployeeDto edit(int id) {
				return dao.findByEmpId(id);
	}

	

	@Override
	public EmployeeDto editSubmit(int id, EmployeeDto dto) {
		EmployeeDto dbobj = dao.findByEmpId(id);
	    dbobj.setFullName(dto.getFullName());
	    dbobj.setAge(dto.getAge());
	    dbobj.setJob(dto.getJob());
	    dbobj.setSalary(dto.getSalary());
	    EmployeeDto save = dao.save(dbobj);
		return save;
	}

	
}
