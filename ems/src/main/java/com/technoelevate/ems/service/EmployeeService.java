package com.technoelevate.ems.service;

import java.util.List;

import com.technoelevate.ems.dto.EmployeeDto;

public interface EmployeeService {
	public EmployeeDto empRegister(EmployeeDto dto);

	public EmployeeDto edit(int id);

	public EmployeeDto editSubmit(int id, EmployeeDto dto);

	
	
}
