package com.technoelevate.ems.service;

import com.technoelevate.ems.dto.AdminDto;
import com.technoelevate.ems.dto.EmployeeDto;

public interface AdminService {
	public AdminDto register(AdminDto dto);

	public AdminDto signIn(AdminDto dto);

	public AdminDto forgotPassword(AdminDto dto);

	


}
