package com.technoelevate.ems.dao;

import org.springframework.data.repository.CrudRepository;

import com.technoelevate.ems.dto.EmployeeDto;


public interface EmployeeDao extends CrudRepository<EmployeeDto, Integer>{
	
	public EmployeeDto findByEmpId(int empId);

}
