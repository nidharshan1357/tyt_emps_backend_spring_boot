package com.technoelevate.ems.dao;

import org.springframework.data.repository.CrudRepository;

import com.technoelevate.ems.dto.AdminDto;

public interface AdminDao extends CrudRepository<AdminDto, String>{
	
	public AdminDto findByEmailId(String emailId);
	

}
