package com.technoelevate.ems.controller;

import java.net.http.HttpRequest;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.technoelevate.ems.dto.AdminDto;
import com.technoelevate.ems.dto.EmployeeDto;
import com.technoelevate.ems.service.AdminService;
import com.technoelevate.ems.service.EmployeeService;

@RestController
public class EmsController {

	@Autowired
	AdminService service;
	
	@Autowired
	EmployeeService empservice;

	@PostMapping(path = "/register")
	public ResponseEntity<?> registerData(@RequestBody AdminDto dto) {
		AdminDto admin = service.register(dto);
		if(admin!=null) {
			return new ResponseEntity<>("data registered sucessfully",HttpStatus.OK);
		}else {
			return new ResponseEntity<>("Both password are not matching please check again ",HttpStatus.INTERNAL_SERVER_ERROR);		
		}
			
		
		
	}
	
	@PostMapping(path = "/signin")
	public ResponseEntity<?> signIn(@RequestBody AdminDto dto) {
		AdminDto dto2 = service.signIn(dto);
		if(dto2==null) {
			return new ResponseEntity<>("Sorry invalid Emaiid please check",HttpStatus.INTERNAL_SERVER_ERROR);	
		}else if(dto2.getPassword().equals(dto.getPassword())) {
							return new ResponseEntity<>("login sucessfully",HttpStatus.OK); 
				
		}
				else {
					return new ResponseEntity<>("Sorry invalid password please check",HttpStatus.INTERNAL_SERVER_ERROR);
			}
		
			
	}
	
	
	
	@PostMapping(path = "/forgotpassword")
	public ResponseEntity<?> forgotPassword(@RequestBody AdminDto dto) {
		AdminDto dto2 = service.forgotPassword(dto);
		if(dto2!=null) {
			return new ResponseEntity<>("password reset sucessfull",HttpStatus.OK); 
		}else {
			return new ResponseEntity<>("Enter valid mail Id:",HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@PostMapping(path = "/addEmployee")
	public ResponseEntity<?> addEmployee(@RequestBody EmployeeDto dto) {
		EmployeeDto res = empservice.empRegister(dto);
		if(res!=null) {
			return new ResponseEntity<>("data registered sucessfully",HttpStatus.OK);
		}else {
			return new ResponseEntity<>("Failed to add employee details please check again ",HttpStatus.INTERNAL_SERVER_ERROR);		
		}
			
		
		
	}
	
	@PostMapping(path = "/edit/{empId}")
	public EmployeeDto edit(@PathVariable ("empId") int id) {
		EmployeeDto res = empservice.edit(id);
		if(res!=null) {
			return res;
		}else {
			return null;
		}
	}
	
	@PostMapping(path = "/editSubmit/{empId}")
	public EmployeeDto editSubmit(@PathVariable ("empId") int id, @RequestBody EmployeeDto dto) {
		EmployeeDto res = empservice.editSubmit(id,dto);
		if(res!=null) {
			return res;
		}else {
			return null;
		}
	}
		
	
	
		

	
	
	
			
	
	
	
	
}
