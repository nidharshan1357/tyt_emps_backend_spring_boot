package com.technoelevate.ems.dto;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "Admin")
public class AdminDto {

	
	
	
	@Id	
	private String emailId;
	private String name;
	private String userName;
	private String password;
	private String confirm_password;

}
