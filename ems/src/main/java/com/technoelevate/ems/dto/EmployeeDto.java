package com.technoelevate.ems.dto;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "Employee")
public class EmployeeDto {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int empId;
	private String fullName;
	private String job;
	private int age;
	private double salary;
	
	
	

}
